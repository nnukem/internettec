
import Swiper from "swiper"

export default class Form{
    constructor(selector){
        this.context = document.querySelector(selector)
        if(!this.context) return
        this.groups = this.context.querySelectorAll(".js-group")    


        this.initSwiper()
        this.initNavigation()
        this.switchToFirstError()
    }

    initSwiper(){
        this.swiper = new Swiper(this.context, { 
            spaceBetween:100,
            allowTouchMove:false,
            autoHeight:true
        });
    }
    initNavigation(){
        this.context.querySelectorAll(".js-next").forEach(btn => {
            btn.addEventListener("click",(e)=>{
                e.preventDefault()
                if(this.validateGroup(this.swiper.activeIndex)){
                    this.swiper.slideNext()
                }
            })
        });

        this.context.querySelectorAll(".js-prev").forEach(btn => {
            btn.addEventListener("click",(e)=>{
                e.preventDefault()
                this.swiper.slidePrev()
            })
        });
        let forced = false
        this.context.querySelector("button[type=submit]").addEventListener('click', e=>{
            if(!forced){
              e.preventDefault()
             // e.stopPropagation()
            }else{
              return
            }
            
            if(this.validateGroup(this.swiper.activeIndex)){
              forced = true;
              e.currentTarget.click();
            }
          }
        )

    }

    switchToFirstError(){
        for(let [i,group] of this.groups.entries()){
            let error = group.querySelector(".is-error")
            if(error) {
                this.swiper.slideTo(i) 
                break;
            }
        }
    }

    validateGroup(index){
        let group = this.groups[index]
     
        let validStd = this.validateStandardFields(group)
        let validChk = this.validateCheckboxes(group)
        let validPass = this.validatePassword(group)

        return validStd && validChk && validPass
    }

    validateStandardFields(ctx){
        let fields = ctx.querySelectorAll("input:not([type=password]),select,textarea")
        let fieldsValidity = true,valid
        fields.forEach((element)=>{
            let parent = element.parentNode
            parent.classList.remove("is-error")
            valid = element.checkValidity()

            if(!valid) parent.classList.add("is-error")
            fieldsValidity = fieldsValidity && valid
        })

        return fieldsValidity
    }

    validateCheckboxes(ctx){
        let fields = ctx.querySelectorAll("input[type=checkbox]")
        let fieldsValidity = true,checkboxes = {}
        fields.forEach((element)=>{
            let parent = element.parentNode.parentNode
            if(!checkboxes[element.name]) checkboxes[element.name] = {valid:false,parent:parent}
            checkboxes[element.name].valid = checkboxes[element.name].valid || element.checked
        })
        for(let checkGroup in checkboxes){
            if(!checkboxes[checkGroup].valid) {
                checkboxes[checkGroup].parent.classList.add("is-error")
            }else{
                checkboxes[checkGroup].parent.classList.remove("is-error")
            }
            fieldsValidity = fieldsValidity && checkboxes[checkGroup].valid
        }

        return fieldsValidity
    }

    validatePassword(ctx){
        let fieldsValidity = true
        let password = ctx.querySelectorAll("input[type=password]")
        
        if(password.length <2) return fieldsValidity

        password[0].parentNode.classList.remove("is-error")
        password[1].parentNode.classList.remove("is-error")

        fieldsValidity = fieldsValidity && password[0].checkValidity()
                

        if(fieldsValidity){
            fieldsValidity = password[0].value === password[1].value
            if(!fieldsValidity)  password[1].parentNode.classList.add("is-error")
        }else{
            password[0].parentNode.classList.add("is-error")
        }

        return fieldsValidity
    }
}