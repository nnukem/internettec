var Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('projectOne', './assets/js/projectOne.js')
    .addEntry('projectTwo', './assets/js/projectTwo.js')
    .addEntry('app', './assets/js/app.js')
    //.addEntry('page1', './assets/js/page1.js')
    //.addEntry('page2', './assets/js/page2.js')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    
    .enableSassLoader(function () {
    }, {resolveUrlLoader: false})
    .copyFiles({
                 from: './assets/img',
        
                 // optional target path, relative to the output dir
                 //to: 'images/[path][name].[ext]',
        
                 // if versioning is enabled, add the file hash too
                 //to: 'images/[path][name].[hash:8].[ext]',
        
                 // only copy files matching this pattern
                 //pattern: /\.(png|jpg|jpeg)$/
             })
    .configureBabel(function (babelConfig) {
        // add additional presets
        const preset = babelConfig.presets.find(([name]) => name === "@babel/preset-env");
        if (preset !== undefined) {
            preset[1].useBuiltIns = "usage";
            preset[1].corejs = 3;
            preset[1].modules = 'commonjs';
            preset[1].debug = false;
            preset[1].targets = {
                'chrome': 52,
                'firefox': 44,
                'safari': 7,
                'ie': 11,
                'edge': 14
            };
        }
    }, {
        // node_modules is not processed through Babel by default
        // but you can whitelist specific modules to process
        includeNodeModules: ['swiper', 'dom7','intersection-observer']

    })
    // enables Sass/SCSS support
    //.enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    // uncomment if you're having problems with a jQuery plugin
    //.autoProvidejQuery()

    // uncomment if you use API Platform Admin (composer req api-admin)
    //.enableReactPreset()
    //.addEntry('admin', './assets/js/admin.js')
;
let config = Encore.getWebpackConfig();

// config.watchOptions = {
//     poll: true,
//     ignored: /node_modules/
// };

config.plugins.push(
    new BrowserSyncPlugin({
        host: 'localhost',
        port: 4600,
        proxy: 'http://localhost:8000',
        files: [ // watch on changes
            {
                match: [
                    'public/build/**/*.js',
                    "templates/**/*twig",
                    "templates/data/**/*json"
                ],
                fn: function (event, file) {
                    if (event === 'change') {
                        const bs = require('browser-sync').get('bs-webpack-plugin');
                        bs.reload();
                    }
                }
            }
        ]
    }, {
        reload: false, // this allow webpack server to take care of instead browser sync
        name: 'bs-webpack-plugin'
    })
);

if (!Encore.isProduction()) {
    config.devtool = 'cheap-eval-source-map';
}

module.exports = config;
