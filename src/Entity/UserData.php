<?php

namespace App\Entity;

use App\Repository\UserDataRepository;
use App\Utils\Errors;
use Doctrine\ORM\Mapping as ORM;
use PhpParser\Node\Expr\ErrorSuppress;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserDataRepository::class)
 */
class UserData
{

    public function parseReq($request){
        $this->setName($request->get("name"));
        $this->setSurname($request->get("surname"));
        $this->setLogin($request->get("login"));
        $this->setEmail($request->get("email"));
        $this->setCity($request->get("city"));
        $this->setStreet($request->get("street"));
        $this->setHouseNumber($request->get("housenumber"));
        $this->setApartmentNumber($request->get("apartmentnumber"));
        $this->setPostCode($request->get("postcode"));
        $this->setInterests(implode(",",$request->get("interests")));
        $this->setPassword(password_hash($request->get("password"), PASSWORD_ARGON2I));
        $this->setEducation($request->get("education"));
        $this->setPlainPassword($request->get("password"));
        $this->setPlainPassword2($request->get("passwordrepeat"));
    }

    public function validate($doc){
        $mailRx = "/^[a-zA-Z0-9.!#$%&’*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/";
        $postRx = "/[0-9]{2}-[0-9]{3}/";
        $passRx = "/^(?=.*[A-Z]{1,})(?=.*[!@#$%^&*]{1,})(?=.*[0-9]{1,})(?=.*[a-z]{1,})([^\s]*){8,}$/";

        $valid = true;

        if(strlen($this->name)<3){
            $this->errors->setNameError(true);
            $valid = false; 
        }
        if(strlen($this->surname)<3){
            $this->errors->setSurnameError(true);
            $valid = false; 
        }
        if(strlen($this->street)<3){
            $this->errors->setStreetError(true);
            $valid = false; 
        }
        if(strlen($this->city)<3){
            $this->errors->setCityError(true);
            $valid = false; 
        }
        if(strlen($this->houseNumber)<1){
            $this->errors->setHouseNumberError(true);
            $valid = false; 
        }
        if((int)$this->education < 0 || (int)$this->education > 2){
            $this->errors->setEducationError(true);
            $valid = false; 
        }
        if(count(explode(",",$this->interests)) < 1 ){
            $this->errors->setInterestsError(true);
            $valid = false; 
        }
        if(strlen($this->login)<3){
            $this->errors->setLoginError(true); 
            $valid = false; 
        }else{
            $tmpUser = $doc->getRepository(UserData::class)
                           ->findByLogin($this->login);
            if(count($tmpUser) >0){
                $this->errors->setLoginErrorMsg("Podany login już istnieje");
                $this->errors->setLoginError(true);
                $valid = false; 
            }                
        }
        if(!preg_match($mailRx,$this->email)){
            $this->errors->setEmailError(true);
            $valid = false; 
        }
        if(!preg_match($postRx,$this->postCode)){
            $this->errors->setPostCodeError(true);
            $valid = false; 
        }
        if(!preg_match($passRx,$this->plainPassword)){
            $this->errors->setPasswordError(true);
            $valid = false; 
        }else{
            if($this->plainPassword != $this->plainPassword2){
                $this->errors->setPasswordRepeatError(true);
                $valid = false; 
            }

        }
        return $valid;
    }

    private $errors;

    public function __construct()
    {
        $this->errors = new Errors();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $surname;

     /**
     * @ORM\Column(type="string", length=64)
     */
    private $login;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $houseNumber;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $apartmentNumber;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $postCode;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $city;

    /**
     * @ORM\Column(type="text")
     */
    private $interests;

    /**
     * @ORM\Column(type="smallint")
     */
    private $education;

    /**
     * @ORM\Column(type="text")
     */
    private $password;

    private $plainPassword;
    private $plainPassword2;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getApartmentNumber(): ?string
    {
        return $this->apartmentNumber;
    }

    public function setApartmentNumber(string $apartmentNumber): self
    {
        $this->apartmentNumber = $apartmentNumber;

        return $this;
    }

    public function getPostCode(): ?string
    {
        return $this->postCode;
    }

    public function setPostCode(string $postCode): self
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getInterests(): ?string
    {
        return $this->interests;
    }

    public function setInterests(string $interests): self
    {
        $this->interests = $interests;

        return $this;
    }

    /**
     * Get the value of login
     */ 
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set the value of login
     *
     * @return  self
     */ 
    public function setLogin(string $login) : self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of errors
     */ 
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get the value of education
     */ 
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * Set the value of education
     *
     * @return  self
     */ 
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get the value of plainPassword2
     */ 
    public function getPlainPassword2()
    {
        return $this->plainPassword2;
    }

    /**
     * Set the value of plainPassword2
     *
     * @return  self
     */ 
    public function setPlainPassword2($plainPassword2)
    {
        $this->plainPassword2 = $plainPassword2;

        return $this;
    }

    /**
     * Get the value of plainPassword
     */ 
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Set the value of plainPassword
     *
     * @return  self
     */ 
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }
}
