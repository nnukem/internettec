<?php

namespace App\Utils;


class Errors
{
   private $nameErrorMsg = "Zbyt krótkie imię (minimum 3 znaki)";
   private $surnameErrorMsg = "Zbyt krótkie nazwisko (minimum 3 znaki)";
   private $loginErrorMsg = "Zbyt krótki login (minimum 3 znaki)";
   private $emailErrorMsg = "Nieprawidłowy adres e-mail";
   private $streetErrorMsg="Zbyt krótka ulica (minimum 3 znaki)";
   private $cityErrorMsg = "Zbyt krótka nazwa miasta (minimum 3 znaki)";
   private $houseNumberErrorMsg = "Brak numeru";
   private $apartmentNumberErrorMsg = "Brak numeru";
   private $postCodeErrorMsg = "Nieprawidłowy kod pocztowy (format XX-XXX)";
   private $passwordErrorMsg = "Podane hasło nie spełnia kryteriów";
   private $passwordRepeatErrorMsg = "Błędnie powtórzone hasło";
   private $educationErrorMsg = "Wybierz wykształcenie";
   private $interestsErrorMsg = "Wybierz co namniej jedno zainteresowanie";

   private $nameError;
   private $surnameError;
   private $loginError;
   private $emailError;
   private $streetError;
   private $cityError;
   private $houseNumberError;
   private $apartmentNumberError;
   private $postCodeError;
   private $passwordRepeatError;
   private $passwordError;
   private $educationError;
   private $interestsError;


   /**
    * Get the value of interestsError
    */ 
   public function getInterestsError()
   {
      return $this->interestsError;
   }

   /**
    * Set the value of interestsError
    *
    * @return  self
    */ 
   public function setInterestsError($interestsError)
   {
      $this->interestsError = $interestsError;

      return $this;
   }

   /**
    * Get the value of nameError
    */ 
   public function getNameError()
   {
      return $this->nameError;
   }

   /**
    * Set the value of nameError
    *
    * @return  self
    */ 
   public function setNameError($nameError)
   {
      $this->nameError = $nameError;

      return $this;
   }

   /**
    * Get the value of surnameError
    */ 
   public function getSurnameError()
   {
      return $this->surnameError;
   }

   /**
    * Set the value of surnameError
    *
    * @return  self
    */ 
   public function setSurnameError($surnameError)
   {
      $this->surnameError = $surnameError;

      return $this;
   }

   /**
    * Get the value of loginError
    */ 
   public function getLoginError()
   {
      return $this->loginError;
   }

   /**
    * Set the value of loginError
    *
    * @return  self
    */ 
   public function setLoginError($loginError)
   {
      $this->loginError = $loginError;

      return $this;
   }

   /**
    * Get the value of educationError
    */ 
   public function getEducationError()
   {
      return $this->educationError;
   }

   /**
    * Set the value of educationError
    *
    * @return  self
    */ 
   public function setEducationError($educationError)
   {
      $this->educationError = $educationError;

      return $this;
   }

   /**
    * Get the value of postCodeError
    */ 
   public function getPostCodeError()
   {
      return $this->postCodeError;
   }

   /**
    * Set the value of postCodeError
    *
    * @return  self
    */ 
   public function setPostCodeError($postCodeError)
   {
      $this->postCodeError = $postCodeError;

      return $this;
   }

   /**
    * Get the value of apartmentNumberError
    */ 
   public function getApartmentNumberError()
   {
      return $this->apartmentNumberError;
   }

   /**
    * Set the value of apartmentNumberError
    *
    * @return  self
    */ 
   public function setApartmentNumberError($apartmentNumberError)
   {
      $this->apartmentNumberError = $apartmentNumberError;

      return $this;
   }

   /**
    * Get the value of cityError
    */ 
   public function getCityError()
   {
      return $this->cityError;
   }

   /**
    * Set the value of cityError
    *
    * @return  self
    */ 
   public function setCityError($cityError)
   {
      $this->cityError = $cityError;

      return $this;
   }

   /**
    * Get the value of houseNumberError
    */ 
   public function getHouseNumberError()
   {
      return $this->houseNumberError;
   }

   /**
    * Set the value of houseNumberError
    *
    * @return  self
    */ 
   public function setHouseNumberError($houseNumberError)
   {
      $this->houseNumberError = $houseNumberError;

      return $this;
   }

   /**
    * Get the value of streetError
    */ 
   public function getStreetError()
   {
      return $this->streetError;
   }

   /**
    * Set the value of streetError
    *
    * @return  self
    */ 
   public function setStreetError($streetError)
   {
      $this->streetError = $streetError;

      return $this;
   }

   /**
    * Get the value of emailError
    */ 
   public function getEmailError()
   {
      return $this->emailError;
   }

   /**
    * Set the value of emailError
    *
    * @return  self
    */ 
   public function setEmailError($emailError)
   {
      $this->emailError = $emailError;

      return $this;
   }

   /**
    * Get the value of nameErrorMsg
    */ 
   public function getNameErrorMsg()
   {
      return $this->nameErrorMsg;
   }

   /**
    * Set the value of nameErrorMsg
    *
    * @return  self
    */ 
   public function setNameErrorMsg($nameErrorMsg)
   {
      $this->nameErrorMsg = $nameErrorMsg;

      return $this;
   }

   /**
    * Get the value of surnameErrorMsg
    */ 
   public function getSurnameErrorMsg()
   {
      return $this->surnameErrorMsg;
   }

   /**
    * Set the value of surnameErrorMsg
    *
    * @return  self
    */ 
   public function setSurnameErrorMsg($surnameErrorMsg)
   {
      $this->surnameErrorMsg = $surnameErrorMsg;

      return $this;
   }

   /**
    * Get the value of loginErrorMsg
    */ 
   public function getLoginErrorMsg()
   {
      return $this->loginErrorMsg;
   }

   /**
    * Set the value of loginErrorMsg
    *
    * @return  self
    */ 
   public function setLoginErrorMsg($loginErrorMsg)
   {
      $this->loginErrorMsg = $loginErrorMsg;

      return $this;
   }

   /**
    * Get the value of emailErrorMsg
    */ 
   public function getEmailErrorMsg()
   {
      return $this->emailErrorMsg;
   }

   /**
    * Set the value of emailErrorMsg
    *
    * @return  self
    */ 
   public function setEmailErrorMsg($emailErrorMsg)
   {
      $this->emailErrorMsg = $emailErrorMsg;

      return $this;
   }

   /**
    * Get the value of streetErrorMsg
    */ 
   public function getStreetErrorMsg()
   {
      return $this->streetErrorMsg;
   }

   /**
    * Set the value of streetErrorMsg
    *
    * @return  self
    */ 
   public function setStreetErrorMsg($streetErrorMsg)
   {
      $this->streetErrorMsg = $streetErrorMsg;

      return $this;
   }

   /**
    * Get the value of cityErrorMsg
    */ 
   public function getCityErrorMsg()
   {
      return $this->cityErrorMsg;
   }

   /**
    * Set the value of cityErrorMsg
    *
    * @return  self
    */ 
   public function setCityErrorMsg($cityErrorMsg)
   {
      $this->cityErrorMsg = $cityErrorMsg;

      return $this;
   }

   /**
    * Get the value of houseNumberErrorMsg
    */ 
   public function getHouseNumberErrorMsg()
   {
      return $this->houseNumberErrorMsg;
   }

   /**
    * Set the value of houseNumberErrorMsg
    *
    * @return  self
    */ 
   public function setHouseNumberErrorMsg($houseNumberErrorMsg)
   {
      $this->houseNumberErrorMsg = $houseNumberErrorMsg;

      return $this;
   }

   /**
    * Get the value of apartmentNumberErrorMsg
    */ 
   public function getApartmentNumberErrorMsg()
   {
      return $this->apartmentNumberErrorMsg;
   }

   /**
    * Set the value of apartmentNumberErrorMsg
    *
    * @return  self
    */ 
   public function setApartmentNumberErrorMsg($apartmentNumberErrorMsg)
   {
      $this->apartmentNumberErrorMsg = $apartmentNumberErrorMsg;

      return $this;
   }

   /**
    * Get the value of postCodeErrorMsg
    */ 
   public function getPostCodeErrorMsg()
   {
      return $this->postCodeErrorMsg;
   }

   /**
    * Set the value of postCodeErrorMsg
    *
    * @return  self
    */ 
   public function setPostCodeErrorMsg($postCodeErrorMsg)
   {
      $this->postCodeErrorMsg = $postCodeErrorMsg;

      return $this;
   }

   /**
    * Get the value of educationErrorMsg
    */ 
   public function getEducationErrorMsg()
   {
      return $this->educationErrorMsg;
   }

   /**
    * Set the value of educationErrorMsg
    *
    * @return  self
    */ 
   public function setEducationErrorMsg($educationErrorMsg)
   {
      $this->educationErrorMsg = $educationErrorMsg;

      return $this;
   }

   /**
    * Get the value of interestsErrorMsg
    */ 
   public function getInterestsErrorMsg()
   {
      return $this->interestsErrorMsg;
   }

   /**
    * Set the value of interestsErrorMsg
    *
    * @return  self
    */ 
   public function setInterestsErrorMsg($interestsErrorMsg)
   {
      $this->interestsErrorMsg = $interestsErrorMsg;

      return $this;
   }

   /**
    * Get the value of passwordErrorMsg
    */ 
   public function getPasswordErrorMsg()
   {
      return $this->passwordErrorMsg;
   }

   /**
    * Set the value of passwordErrorMsg
    *
    * @return  self
    */ 
   public function setPasswordErrorMsg($passwordErrorMsg)
   {
      $this->passwordErrorMsg = $passwordErrorMsg;

      return $this;
   }

   /**
    * Get the value of passwordError
    */ 
   public function getPasswordError()
   {
      return $this->passwordError;
   }

   /**
    * Set the value of passwordError
    *
    * @return  self
    */ 
   public function setPasswordError($passwordError)
   {
      $this->passwordError = $passwordError;

      return $this;
   }

   /**
    * Get the value of passwordRepeatError
    */ 
   public function getPasswordRepeatError()
   {
      return $this->passwordRepeatError;
   }

   /**
    * Set the value of passwordRepeatError
    *
    * @return  self
    */ 
   public function setPasswordRepeatError($passwordRepeatError)
   {
      $this->passwordRepeatError = $passwordRepeatError;

      return $this;
   }

   /**
    * Get the value of passwordRepeatErrorMsg
    */ 
   public function getPasswordRepeatErrorMsg()
   {
      return $this->passwordRepeatErrorMsg;
   }

   /**
    * Set the value of passwordRepeatErrorMsg
    *
    * @return  self
    */ 
   public function setPasswordRepeatErrorMsg($passwordRepeatErrorMsg)
   {
      $this->passwordRepeatErrorMsg = $passwordRepeatErrorMsg;

      return $this;
   }
}
