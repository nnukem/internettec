<?php

namespace App\Controller;

use App\Entity\UserData;
use App\Utils\Errors;
use PhpParser\Node\Name;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MainController extends AbstractController
{

    /**
     * @Route("/",name="home")
     * @Route("/projekt-1",name="html-form")
     */
    public function form(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $formData = new UserData(); 

        if($request->isMethod("POST")){
            $formData->parseReq($request->request);
            $doc = $this->getDoctrine();     

            if($formData->validate($doc)){
                
                $docm = $doc->getManager();
                $docm->persist($formData);
                $docm->flush(); 

                return $this->render('pages/projectOne/form-thanks.html.twig', [
                    "title"=>"Dziękujemy za wysłanie formularza",
                    "data"=>$formData
                ]);
            }else{
                return $this->render('pages/projectOne/form.html.twig', [
                    "title"=>"Formularz rejestracyjny",
                    "data" => $formData
                ]);
            }
            
        }

        return $this->render('pages/projectOne/form.html.twig', [
            "title"=>"Formularz rejestracyjny",
            "data" => $formData
        ]);
    }


    /**
     * @Route("/info",name="info")
     */
    public function info()
    {
        ob_start();
        phpinfo();
        $phpinfo = ob_get_clean();

        return new Response(
            '<html><body>'.$phpinfo.'</body></html>'
        );
    }
    private function clearInput($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}
